from playwright.sync_api import Playwright, sync_playwright, expect


def run(playwright: Playwright) -> None:
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()

    # Open new page
    page = context.new_page()

    # Go to https://www.baidu.com/
    page.goto("https://www.baidu.com/")

    # Click input[name="wd"]
    page.locator("input[name=\"wd\"]").click()

    # Fill input[name="wd"]
    page.locator("input[name=\"wd\"]").fill("suisei")

    # Click text=百度一下
    page.locator("text=百度一下").click()
    page.wait_for_url("https://www.baidu.com/s?ie=utf-8&f=8&rsv_bp=1&rsv_idx=1&tn=baidu&wd=suisei&fenlei=256&rsv_pq=ab642490000176b6&rsv_t=dcac2wkGBrO8hgygZn8XbHkW73uMvI6zittILApgp30y5RupmgDE61G2WVU&rqlang=cn&rsv_enter=0&rsv_dl=tb&rsv_sug3=7&rsv_sug1=6&rsv_sug7=100&rsv_btype=i&prefixsug=suisei&rsp=1&inputT=3931&rsv_sug4=7191&rsv_jmp=fail")

    # Click #container
    page.locator("#container").click()

    # Click text=/.*"suisei"是什么意思\? -关于日语\(日文\) \| HiNative.*/
    with page.expect_popup() as popup_info:
        page.locator("text=/.*\"suisei\"是什么意思\\? -关于日语\\(日文\\) \\| HiNative.*/").click()
    page1 = popup_info.value
    page.wait_for_url("https://zh.hinative.com/questions/1221921")

    # Click #answer-3040327 > .chat_content_wrapper > .row > .container_fukidashi > .body_whole_bubble > .container_whole_bubble > .wrapper_fukidashi > .answer_links > .answer_content > .mod_toolbuttons > div > .operation > .toolbutton_item > .mod_dropdown > .toolbutton_anchor > .hs
    page1.locator("#answer-3040327 > .chat_content_wrapper > .row > .container_fukidashi > .body_whole_bubble > .container_whole_bubble > .wrapper_fukidashi > .answer_links > .answer_content > .mod_toolbuttons > div > .operation > .toolbutton_item > .mod_dropdown > .toolbutton_anchor > .hs").click()

    # Close page
    page1.close()

    # Close page
    page.close()

    # ---------------------
    context.close()
    browser.close()


with sync_playwright() as playwright:
    run(playwright)
